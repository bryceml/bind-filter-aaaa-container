#!/bin/sh

cat > /etc/bind/named.conf.options <<EOF
options {
  forwarders {
    ${FORWARDER};
  };
  allow-query { any; };
  allow-query-cache { any; };
  allow-recursion { any; };
  dnssec-validation no;
  filter-aaaa-on-v4 break-dnssec;
  filter-aaaa-on-v6 break-dnssec;
  forward only;
};
EOF

exec named -f
