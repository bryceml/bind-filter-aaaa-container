FROM debian:buster-slim

ENV TERM=screen-256color
ENV LANG=C.UTF-8
RUN apt-get update; apt-get install -y dumb-init bind9; rm -rf /var/lib/apt/lists
EXPOSE 53
EXPOSE 53/udp
ENTRYPOINT ["/usr/bin/dumb-init", "/run.sh"]
ADD run.sh /
